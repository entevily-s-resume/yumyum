﻿using System;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField]
    private float offScreenXDistance =-4f;

    private Rigidbody rb;
    private Quaternion startingRotation;

    private bool firstSpawn = true;


    public void Spawn(Vector3 position)
    {
        gameObject.SetActive(true);
        if (firstSpawn)
        {
            startingRotation = transform.rotation;
        }

        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = new Vector3(0f, 0f, 0f);
        transform.rotation = startingRotation;
        GetComponent<ItemMover>().enabled = true;
        
        float curZed = position.z;
        float rnd = UnityEngine.Random.Range(-.099f, .099f);
        curZed += rnd;
        position.z = curZed;
        transform.position = position;
    }

    private void Update()
    {
        // Return object to pool when it's off bottom of screen or off left of screen.
        if(transform.position.x < offScreenXDistance || transform.position.y <= -10)
        {
            Return();
        }
        
        if (rb.useGravity)
        {
            GameManager.Instance.yum.anim.SetBool("item-flying", true);
            if (GameManager.Instance.yum.transform.position.x < transform.position.x)
                GameManager.Instance.yum.anim.SetBool("flying-left", false);
            else
                GameManager.Instance.yum.anim.SetBool("flying-left", true);
        }
    }

    internal void Return()
    {
        GameManager.Instance.yum.anim.SetBool("item-flying", false);
        GetComponent<ItemThrower>().Reset();
        GameManager.Instance.poolManager.ReturnObject(this);
    }
}