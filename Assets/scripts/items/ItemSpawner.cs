﻿using UnityEngine;

public class ItemSpawner : MonoBehaviour
{
    [SerializeField]
    private float spawnRateInSeconds = 10f;

    private float counter = 0f;

    private void Update()
    {
        Debug.Log(spawnRateInSeconds);
        Debug.Log(GameManager.Instance.DifficultyPercent);
        var rater = spawnRateInSeconds - ( GameManager.Instance.DifficultyPercent);
        counter += Time.deltaTime;
        if(counter >= rater)
        {
            SpawnItem();
            counter = 0;
        }
    }

    private void SpawnItem()
    {
        var item = GameManager.Instance.poolManager.Get();
        if (item == null)
            return;
        
        item.Spawn(transform.position);
    }
}
