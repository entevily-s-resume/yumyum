﻿using UnityEngine;

public class ItemMover : MonoBehaviour
{
    private void Update()
    {
        Convey();
    }

    private void Convey()
    {
            transform.Translate(Vector3.left * Time.deltaTime * GameManager.Instance.conveyorSpeed/2, Space.World);

    }
}
