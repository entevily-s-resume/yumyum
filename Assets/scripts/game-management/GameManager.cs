﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    public static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("GameManager");
                    _instance = container.AddComponent<GameManager>();
                }
            }
            return _instance;
        }
    }
    #endregion
    [Header("Item Pools")]
    public ObjectPoolManager poolManager;

    public List<Sound> eatSounds = new List<Sound>();
    public List<Sound> sickSounds = new List<Sound>();
    public List<Sound> angrySounds = new List<Sound>();
    public List<Sound> yumyumSounds = new List<Sound>();

    public YumYum yum;
    public GameObject retryButton;
    private bool gameCompleted = false;

    public void ShowRetry()
    {
        retryButton.SetActive(true);
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public float conveyorSpeed { get { return 5 * DifficultyPercent; } }


    public float DifficultyPercent { get { return yum.ScalePrecentage; } }

    internal void PlayEat()
    {
        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(eatSounds[UnityEngine.Random.Range(0, eatSounds.Count)],"eat");
    }

    internal void PlaySick()
    {
        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(sickSounds[UnityEngine.Random.Range(0, sickSounds.Count)], "sick");
    }

    internal void PlayYum()
    {
        if (GameObject.Find("sound:yum"))
            return;
        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(yumyumSounds[UnityEngine.Random.Range(0, yumyumSounds.Count)], "yum");
    }

    internal void PlayAngry()
    {
        AudioManager.AUDIO_MANAGER.PlayAudioGlobal(angrySounds[UnityEngine.Random.Range(0, angrySounds.Count)], "angry");
    }

    internal void RollCredits()
    {
        SceneManager.LoadScene("credits");
    }

    internal void LoadOutro()
    {
        SceneManager.LoadScene("tape-recorder1");
    }
}

