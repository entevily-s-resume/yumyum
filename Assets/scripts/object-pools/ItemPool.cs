﻿using UnityEngine;

public class ItemPool : ObjectPool<Item>
{
    internal void Send(Item item, int count)
    {
        m_prefab = item;
        m_size = count;
        Awake();
    }
}