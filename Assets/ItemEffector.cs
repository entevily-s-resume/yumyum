﻿using UnityEngine;

public class ItemEffector : MonoBehaviour
{
    public enum Effector
    {
        HEAL,
        HURT,
        HUNGER,
        QUENCH, 
        NONE
    }

    public Effector primaryEffect;
    public Effector secondaryEffect;
}
