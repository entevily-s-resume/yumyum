﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAdjuster : MonoBehaviour
{
    private float largest = 39;
    private float smallest = 21;

    private void Update()
    {
        var newSize = GameManager.Instance.yum.ScalePrecentage * (largest - smallest);
        Debug.Log(newSize);
        newSize = largest - newSize;
        newSize = Mathf.Clamp(newSize, smallest, largest);

        GetComponent<Camera>().focalLength = Mathf.Lerp(GetComponent<Camera>().focalLength, newSize, 5* Time.deltaTime);
    }
}
